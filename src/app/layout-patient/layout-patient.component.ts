import { Component, OnInit } from '@angular/core';
import { Patient } from '../models/patient';
import { PatientService } from '../services/patient.service';

@Component({
  selector: 'app-layout-patient',
  templateUrl: './layout-patient.component.html',
  styleUrls: ['./layout-patient.component.css']
})
export class LayoutPatientComponent implements OnInit {
  patients:Patient[] = [];
  constructor(private patientService:PatientService) { }

  ngOnInit(): void {
    this.patientService.findAll().subscribe((patients:Patient[])=>{
      this.patients = patients;
    })
  }

}
