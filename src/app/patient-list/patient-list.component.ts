import { Component, Input, OnInit } from '@angular/core';
import { Patient } from '../models/patient';
import { PatientService } from '../services/patient.service';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {
  @Input() patients!:Patient[]
  constructor(private patientService:PatientService) { }

  ngOnInit(): void {
  }

  supprimer = (id:Number) =>{
    this.patients = this.patients.filter((patient)=>{
      if(patient.id==id){
          this.patientService.delete(id).subscribe();
      }
      else{
        return patient;
      }
      return "";
    })
  }
}
