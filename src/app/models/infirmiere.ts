import { Adresse } from "./adresse";

export interface Infirmiere {
    id:Number,
    numeroProfessionnel:Number,
    nom:String,
    prenom:String,
    telPro:Number,
    telPerso:Number,
    adresse:Adresse
}
