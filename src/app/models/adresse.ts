export interface Adresse {
    id:Number,
    numero:String,
    rue:String,
    ville:String,
    cp:String
}
