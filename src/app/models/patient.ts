import { Adresse } from "./adresse";
import { Infirmiere } from "./infirmiere";

export enum Sexe{
    Homme="Homme",
    Femme="Femme",
    Autre="Autre"
}

export interface Patient {
    id:Number,
    sexe:Sexe,
    nom:String,
    prenom:String,
    dateDeNaissance:Date,
    numeroSecuriteSociale:Number,
    infirmiere:Infirmiere,
    adresse:Adresse
}
