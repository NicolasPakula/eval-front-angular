import { Component, Input, OnInit } from '@angular/core';
import { Infirmiere } from '../models/infirmiere';
import { InfirmiereServiceService } from '../services/infirmiere-service.service';

@Component({
  selector: 'app-infirmiere-list',
  templateUrl: './infirmiere-list.component.html',
  styleUrls: ['./infirmiere-list.component.css']
})
export class InfirmiereListComponent implements OnInit {
  @Input() infirmieres!:Infirmiere[]
  constructor(private infirmiereService:InfirmiereServiceService) { }

  ngOnInit(): void {
  }

  supprimer = (id:Number) =>{
    this.infirmieres = this.infirmieres.filter((infirmiere)=>{
      if(infirmiere.id==id){
          this.infirmiereService.delete(id).subscribe();
      }
      else{
        return infirmiere;
      }
      return "";
    })
  }

}
