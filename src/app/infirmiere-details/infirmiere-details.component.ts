import { Component, Input, OnInit } from '@angular/core';
import { Infirmiere } from '../models/infirmiere';

@Component({
  selector: 'app-infirmiere-details',
  templateUrl: './infirmiere-details.component.html',
  styleUrls: ['./infirmiere-details.component.css']
})
export class InfirmiereDetailsComponent implements OnInit {
  @Input() infirmiere!:Infirmiere;

  constructor() { }

  ngOnInit(): void {
    
  }

}
