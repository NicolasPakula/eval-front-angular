import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfirmiereFormulaireComponent } from './infirmiere-formulaire.component';

describe('InfirmiereFormulaireComponent', () => {
  let component: InfirmiereFormulaireComponent;
  let fixture: ComponentFixture<InfirmiereFormulaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfirmiereFormulaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfirmiereFormulaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
