import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InfirmiereServiceService } from '../services/infirmiere-service.service';

@Component({
  selector: 'app-infirmiere-formulaire',
  templateUrl: './infirmiere-formulaire.component.html',
  styleUrls: ['./infirmiere-formulaire.component.css']
})
export class InfirmiereFormulaireComponent implements OnInit {
  infirmiere: FormGroup;
  constructor(fb: FormBuilder,private infirmiereService:InfirmiereServiceService,private router: Router) { 
    this.infirmiere = fb.group({
      nom: ["", Validators.required],
      prenom: ["", Validators.required],
      numeroProfessionnel: ["", Validators.required],
      telPro: ["", Validators.required],
      telPerso: ["", Validators.required],
      adresse: fb.group({
        numero: ["", Validators.required],
        rue: ["", Validators.required],
        ville: ["", Validators.required],
        cp: ["", Validators.required],
      })
    });
  }

  ngOnInit(): void {
  }

  envoyerFormulaire():void{
    console.log(this.infirmiere.value);
    this.infirmiereService.create(this.infirmiere.value).subscribe();
  }
}
