import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Patient } from '../models/patient';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http:HttpClient) { }

  findAll(): Observable<Patient[]>{
    return this.http.get<Patient[]>(`${environment.api_url}/patients`);
  }

  create(patient:Patient):Observable<Patient>{
    return this.http.post<Patient>(`${environment.api_url}/patients`,patient);
  }

  delete(id:Number){
    return this.http.delete<Patient>(`${environment.api_url}/patients/${id}`);
  }

  findById(id:Number):Observable<Patient>{
    return this.http.get<Patient>(`${environment.api_url}/patients/${id}`);
  }
}
