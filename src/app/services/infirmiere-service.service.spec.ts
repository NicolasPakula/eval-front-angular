import { TestBed } from '@angular/core/testing';

import { InfirmiereServiceService } from './infirmiere-service.service';

describe('InfirmiereServiceService', () => {
  let service: InfirmiereServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InfirmiereServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
