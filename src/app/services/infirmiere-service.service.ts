import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Infirmiere } from '../models/infirmiere';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InfirmiereServiceService {

  constructor(private http:HttpClient) { }

  findAll(): Observable<Infirmiere[]>{
    return this.http.get<Infirmiere[]>(`${environment.api_url}/infirmieres`);
  }

  create(infirmiere:Infirmiere):Observable<Infirmiere>{
    return this.http.post<Infirmiere>(`${environment.api_url}/infirmieres`,infirmiere);
  }

  delete(id:Number){
    return this.http.delete<Infirmiere>(`${environment.api_url}/infirmieres/${id}`);
  }

  findById(id:Number):Observable<Infirmiere>{
    return this.http.get<Infirmiere>(`${environment.api_url}/infirmieres/${id}`);
  }
}
