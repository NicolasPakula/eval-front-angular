import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Patient } from '../models/patient';
import { PatientService } from '../services/patient.service';

@Component({
  selector: 'app-layout-patient-details',
  templateUrl: './layout-patient-details.component.html',
  styleUrls: ['./layout-patient-details.component.css']
})
export class LayoutPatientDetailsComponent implements OnInit {
  patient!:Patient;
  constructor(private route:ActivatedRoute,private patientService:PatientService) { }

  ngOnInit(): void {
    if(Number(this.route.snapshot.paramMap.get('id')!=null)){
      this.patientService.findById(Number(this.route.snapshot.paramMap.get('id'))).subscribe((patient)=>{
        this.patient = patient;
      });
    }
  }

}
