import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutPatientDetailsComponent } from './layout-patient-details.component';

describe('LayoutPatientDetailsComponent', () => {
  let component: LayoutPatientDetailsComponent;
  let fixture: ComponentFixture<LayoutPatientDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutPatientDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutPatientDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
