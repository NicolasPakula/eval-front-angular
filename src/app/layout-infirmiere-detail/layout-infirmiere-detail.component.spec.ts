import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutInfirmiereDetailComponent } from './layout-infirmiere-detail.component';

describe('LayoutInfirmiereDetailComponent', () => {
  let component: LayoutInfirmiereDetailComponent;
  let fixture: ComponentFixture<LayoutInfirmiereDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutInfirmiereDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutInfirmiereDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
