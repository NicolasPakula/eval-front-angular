import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Infirmiere } from '../models/infirmiere';
import { InfirmiereServiceService } from '../services/infirmiere-service.service';

@Component({
  selector: 'app-layout-infirmiere-detail',
  templateUrl: './layout-infirmiere-detail.component.html',
  styleUrls: ['./layout-infirmiere-detail.component.css']
})
export class LayoutInfirmiereDetailComponent implements OnInit {
  infirmiere!:Infirmiere;
  
  constructor(private route: ActivatedRoute,private infirmiereService:InfirmiereServiceService) { }

  ngOnInit(): void {
    if(Number(this.route.snapshot.paramMap.get('id')!=null)){
      this.infirmiereService.findById(Number(this.route.snapshot.paramMap.get('id'))).subscribe((infirmiere)=>{
        this.infirmiere = infirmiere;
      });
    }
  }

}
