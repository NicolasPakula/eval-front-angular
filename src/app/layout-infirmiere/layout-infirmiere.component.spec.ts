import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutInfirmiereComponent } from './layout-infirmiere.component';

describe('LayoutInfirmiereComponent', () => {
  let component: LayoutInfirmiereComponent;
  let fixture: ComponentFixture<LayoutInfirmiereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutInfirmiereComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutInfirmiereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
