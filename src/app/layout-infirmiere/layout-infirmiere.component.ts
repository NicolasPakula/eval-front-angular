import { Component, OnInit } from '@angular/core';
import { Infirmiere } from '../models/infirmiere';
import { InfirmiereServiceService } from '../services/infirmiere-service.service';

@Component({
  selector: 'app-layout-infirmiere',
  templateUrl: './layout-infirmiere.component.html',
  styleUrls: ['./layout-infirmiere.component.css']
})
export class LayoutInfirmiereComponent implements OnInit {
  infirmieres:Infirmiere[] = [];

  constructor(private infirmiereService:InfirmiereServiceService) { }

  ngOnInit(): void {
    this.infirmiereService.findAll().subscribe((infirmieres:Infirmiere[])=>{
      this.infirmieres = infirmieres;
    })
  }

}
