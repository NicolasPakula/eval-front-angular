import { Component, Input, OnInit } from '@angular/core';
import { Adresse } from '../models/adresse';

@Component({
  selector: 'app-adresse-details',
  templateUrl: './adresse-details.component.html',
  styleUrls: ['./adresse-details.component.css']
})
export class AdresseDetailsComponent implements OnInit {
  @Input() adresse!:Adresse;
  constructor() { }

  ngOnInit(): void {
  }

}
