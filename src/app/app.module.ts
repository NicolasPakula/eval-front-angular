import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutInfirmiereComponent } from './layout-infirmiere/layout-infirmiere.component';
import { InfirmiereListComponent } from './infirmiere-list/infirmiere-list.component';
import { InfirmiereFormulaireComponent } from './infirmiere-formulaire/infirmiere-formulaire.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfirmiereDetailsComponent } from './infirmiere-details/infirmiere-details.component';
import { AdresseDetailsComponent } from './adresse-details/adresse-details.component';
import { LayoutInfirmiereDetailComponent } from './layout-infirmiere-detail/layout-infirmiere-detail.component';
import { LayoutPatientComponent } from './layout-patient/layout-patient.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { LayoutPatientDetailsComponent } from './layout-patient-details/layout-patient-details.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutInfirmiereComponent,
    InfirmiereListComponent,
    InfirmiereFormulaireComponent,
    InfirmiereDetailsComponent,
    AdresseDetailsComponent,
    LayoutInfirmiereDetailComponent,
    LayoutPatientComponent,
    PatientListComponent,
    PatientDetailsComponent,
    LayoutPatientDetailsComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
