import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfirmiereFormulaireComponent } from './infirmiere-formulaire/infirmiere-formulaire.component';
import { LayoutInfirmiereDetailComponent } from './layout-infirmiere-detail/layout-infirmiere-detail.component';
import { LayoutInfirmiereComponent } from './layout-infirmiere/layout-infirmiere.component';
import { LayoutPatientDetailsComponent } from './layout-patient-details/layout-patient-details.component';
import { LayoutPatientComponent } from './layout-patient/layout-patient.component';

const routes: Routes = [
  {path: "", redirectTo: "infirmieres", pathMatch: "full"},
  {path: "infirmieres", component: LayoutInfirmiereComponent, pathMatch: "full"},
  {path: "infirmieres/ajouter", component: InfirmiereFormulaireComponent, pathMatch: "full"},
  {path: "infirmieres/:id", component: LayoutInfirmiereDetailComponent, pathMatch: "full"},
  {path: "patients", component: LayoutPatientComponent, pathMatch: "full"},
  {path: "patients/:id", component: LayoutPatientDetailsComponent, pathMatch: "full"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
